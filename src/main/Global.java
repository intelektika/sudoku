package main;

import java.awt.*;

import main.objects.*;
import main.ui.SudokuJFrame;

public class Global {
    public static final Dimension cellDimension = new Dimension(60,60);
    public static final Font calibreFont = new Font("Calibre", Font.BOLD, 22);

    public static final Color cellNormal = new Color(210,210,100);
    public static final Color cellHover = new Color(180,180,100);

    public static Sudoku sudoku = new Sudoku();
    public static Generator generator = new Generator(sudoku);
    public static Solver solver = new Solver(sudoku);
    public static SudokuJFrame sudokuJFrame = new SudokuJFrame();
}
