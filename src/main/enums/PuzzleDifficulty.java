package main.enums;

public enum PuzzleDifficulty {
    EASY(10), MEDIUM(25), HARD(40);
    private final int missingNumbers;

    PuzzleDifficulty(int missingNumbers) {
        this.missingNumbers = missingNumbers;
    }

    public int getMissingNumbers() {
        return this.missingNumbers;
    }
}
