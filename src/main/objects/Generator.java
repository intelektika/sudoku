package main.objects;

import main.abstracts.Grid;
import main.abstracts.IGenerator;

public class Generator extends Grid implements IGenerator {
    public Generator(Sudoku sudoku) {
        this.sudoku = sudoku;
    }

    public void generatePuzzle(int missingNumbers) {
        this.sudoku.initBoard();
        String error = "";

        if(missingNumbers >= 81)
            error += "Missing numbers can't be 81 or above!";
        if(missingNumbers <= 0)
            error += "Missing numbers can't be 0 or below!";

        if(!error.equals("")) {
            System.out.println(error);
            return;
        }

        fillDiagonal();
        fillRemaining(0, 0);
        removeDigits(missingNumbers);
    }

    public void fillDiagonal() {
        for (int i = 0; i < sudoku.getSize(); i = i + sudoku.getSizeSqr())
            fillBox(i, i);
    }

    public boolean fillRemaining(int i, int j) {
        if(j >= sudoku.getSize() && i < sudoku.getSize() - 1) {
            i++;
            j = 0;
        }

        if(i >= sudoku.getSize() && j >= sudoku.getSize())
            return true;

        if(i < 3) {
            if (j < 3) {
                j = 3;
            }
        } else if(i < sudoku.getSize() - sudoku.getSizeSqr()) {
            if(j == (i / sudoku.getSizeSqr()) * sudoku.getSizeSqr()) {
                j = j + sudoku.getSizeSqr();
            }
        } else {
            if(j == sudoku.getSize() - sudoku.getSizeSqr()) {
                i++;
                j = 0;
                if(i >= sudoku.getSize())
                    return true;
            }
        }

        for(int num = 1; num <= sudoku.getSize(); num++) {
            if (checkIfSafe(i, j, num)) {
                sudoku.getBoard()[i][j] = num;
                if (fillRemaining(i, j + 1))
                    return true;

                sudoku.getBoard()[i][j] = 0;
            }
        }

        return false;
    }

    public void fillBox(int row, int col) {
        int num;
        for(int i = 0; i < sudoku.getSizeSqr(); i++) {
            for(int j = 0; j < sudoku.getSizeSqr(); j++) {
                do {
                    num = randomNumber(sudoku.getSize());
                } while(!unUsedInBox(row, col, num));
                sudoku.getBoard()[row + i][col + j] = num;
            }
        }
    }

    public void removeDigits(int missingNumbers) {
        int count = missingNumbers;
        while(count != 0) {
            int cellId = randomNumber(sudoku.getSize() * sudoku.getSize()) - 1;
            int i = (cellId / sudoku.getSize());
            int j = cellId % sudoku.getSize();
            if(j != 0)
                j--;
            if(sudoku.getBoard()[i][j] != 0) {
                count--;
                sudoku.getBoard()[i][j] = 0;
            }
        }
    }
}
