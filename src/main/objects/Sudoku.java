package main.objects;

import main.abstracts.Puzzle;

public class Sudoku extends Puzzle {
    public Sudoku() {
        setSize(9);
        setSizeSqr((int) Math.sqrt(getSize()));
        setBoard(new int[getSize()][getSize()]);
        initBoard();
    }
}
