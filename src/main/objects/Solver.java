package main.objects;

import main.Global;
import main.abstracts.Grid;
import main.abstracts.ISolver;

public class Solver extends Grid implements ISolver {
    public Solver(Sudoku sudoku) {
        this.sudoku = sudoku;
    }

    @Override
    public boolean solveSudoku(int row, int col) {
        if(row == sudoku.getSize() - 1 && col == sudoku.getSize())
            return true;

        if(col == sudoku.getSize()) {
            row++;
            col = 0;
        }

        if(sudoku.getBoard()[row][col] != 0)
            return solveSudoku(row, col + 1);

        for(int num = 1; num < 10; num++) {
            if(checkIfSafe(row, col, num)) {
                sudoku.getBoard()[row][col] = num;
                Global.sudokuJFrame.turnCount++;
                Global.sudokuJFrame.updateTurnCount();
                if(solveSudoku(row, col + 1))
                    return true;
            }

            sudoku.getBoard()[row][col] = 0;
        }

        return false;
    }

    @Override
    public boolean placeNumber(int row, int col, int value) {
        if(checkIfSafe(row, col, value)) {
            sudoku.getBoard()[row][col] = value;
            return true;
        }

        return false;
    }
}
