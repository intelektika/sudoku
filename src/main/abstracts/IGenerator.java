package main.abstracts;

public interface IGenerator {
    void generatePuzzle(int missingNumbers);
    void fillDiagonal();
    boolean fillRemaining(int i, int j);
    void fillBox(int row, int col);
    void removeDigits(int missingNumbers);
}
