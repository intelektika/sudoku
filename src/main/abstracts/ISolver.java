package main.abstracts;

public interface ISolver {
    boolean solveSudoku(int row, int col);
    boolean placeNumber(int row, int col, int value);
}
