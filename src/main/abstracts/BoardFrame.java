package main.abstracts;


import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import main.ui.BoardCell;
import main.objects.Solver;
import main.objects.Sudoku;
import main.objects.Generator;
import main.enums.PuzzleDifficulty;

public abstract class BoardFrame extends JFrame {
    protected Container container;
    protected BoardCell[][] squareDisplay;
    protected JLabel turnLabel;
    protected JLabel filledLabel;

    protected static final JRadioButton radioEASY = new JRadioButton("EASY");
    protected static final JRadioButton radioMEDIUM = new JRadioButton("MEDIUM");
    protected static final JRadioButton radioHARD = new JRadioButton("HARD");
    protected ButtonGroup diffGroup = new ButtonGroup();

    protected JButton setDifficulty = new JButton("Set Difficulty");
    protected JButton solveBoard = new JButton("Solve Puzzle");
    protected JButton clearBoard = new JButton("Clear");

    protected Sudoku sudoku;
    protected Generator generator;
    protected Solver solver;

    public int turnCount;

    protected boolean solved;
    protected int filledSlots;

    protected void init() {
        setUpBoard();
        JPanel infoBoard = createInfoBoard();
        infoBoard.add(panelDIFFICULTY());
        infoBoard.add(panelSOLVER());
        infoBoard.add(panelINFO());
        container.add(infoBoard);
        squareStyle();
    }

    protected void setUpBoard() {
        JPanel mapBoard = new JPanel();
        mapBoard.setLayout(new GridLayout(sudoku.getSize(), sudoku.getSize()));
        mapBoard.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 10, 10, 10), new EtchedBorder()));
        squareDisplay = new BoardCell[sudoku.getSize()][sudoku.getSize()];
        for(int y = 0; y < sudoku.getSize(); y++) {
            for (int x = 0; x < sudoku.getSize(); x++) {
                squareDisplay[x][y] = new BoardCell(x, y);
                mapBoard.add(squareDisplay[x][y]);
            }
        }

        container.add(mapBoard);
    }

    protected JPanel createInfoBoard() {
        JPanel infoBoard = new JPanel();
        infoBoard.setLayout(new GridLayout(10, 1));
        return infoBoard;
    }

    protected void clearBoard() {
        sudoku.initBoard();
        refreshBoard();
        turnCount = 0;
        updateTurnCount();
        solved = false;
        filledSlots = 0;
    }

    protected JPanel panelDIFFICULTY() {
        JPanel functionPanel = new JPanel();
        functionPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        functionPanel.setBorder(BorderFactory.createTitledBorder("Difficulty Settings"));

        radioEASY.setActionCommand("EASY");
        radioMEDIUM.setActionCommand("MEDIUM");
        radioHARD.setActionCommand("HARD");

        diffGroup.add(radioEASY);
        diffGroup.add(radioMEDIUM);
        diffGroup.add(radioHARD);

        functionPanel.add(radioEASY);
        functionPanel.add(radioMEDIUM);
        functionPanel.add(radioHARD);
        functionPanel.add(setDifficulty);
        radioEASY.setSelected(true);

        setDifficulty.addActionListener(e -> {
            try {
                generate(diffGroup.getSelection().getActionCommand());
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
        return functionPanel;
    }

    protected JPanel panelSOLVER() {
        JPanel functionPanel = new JPanel(new GridLayout(1, 2));
        functionPanel.setBorder(BorderFactory.createTitledBorder("Solver"));

        functionPanel.add(solveBoard);
        solveBoard.addActionListener(e -> {
            solver.solveSudoku(0, 0);
            refreshBoard();
        });

        functionPanel.add(clearBoard);
        clearBoard.addActionListener(e -> clearBoard());

        return functionPanel;
    }

    protected JPanel panelINFO() {
        JPanel infoPanel = new JPanel();
        infoPanel.setLayout(new GridLayout(1, 2));
        infoPanel.add(panelTURNS());
        infoPanel.add(panelFILLED());
        return infoPanel;
    }

    protected JPanel panelTURNS() {
        JPanel turnPanel = new JPanel();
        turnPanel.setBorder(BorderFactory.createTitledBorder("Turns"));

        turnPanel.add(turnLabel);
        updateTurnCount();

        return turnPanel;
    }

    protected JPanel panelFILLED() {
        JPanel filledPanel = new JPanel();

        filledPanel.setBorder(BorderFactory.createTitledBorder("Slots"));

        filledPanel.add(filledLabel);
        updateFilledCount();

        return filledPanel;
    }

    public void refreshBoard() {
        int filledSlotsUpdate = 0;
        for(int y = 0; y < sudoku.getSize(); y++) {
            for (int x = 0; x < sudoku.getSize(); x++) {
                if (sudoku.getBoard()[x][y] == 0)
                    squareDisplay[x][y].setText("");
                else {
                    squareDisplay[x][y].setText(String.valueOf(sudoku.getBoard()[x][y]));
                    filledSlotsUpdate++;
                }
            }
        }

        this.filledSlots = filledSlotsUpdate;
        updateFilledCount();

        if(filledSlots == 81) {
            this.setEnabled(false);
            JOptionPane.showMessageDialog(this, "Turns taken until completion: " + turnCount, "Congratulations!", JOptionPane.INFORMATION_MESSAGE);
            clearBoard();
            this.setEnabled(true);
        }
    }

    protected void generate(String difficulty) throws Exception {
        switch (difficulty) {
            case "EASY" -> generator.generatePuzzle(PuzzleDifficulty.EASY.getMissingNumbers());
            case "MEDIUM" -> generator.generatePuzzle(PuzzleDifficulty.MEDIUM.getMissingNumbers());
            case "HARD" -> generator.generatePuzzle(PuzzleDifficulty.HARD.getMissingNumbers());
            default -> throw new Exception("Difficulty does not exist!");
        }
        refreshBoard();
    }

    public void updateTurnCount() {
        turnLabel.setText("Turn count: " + turnCount);
    }

    public void updateFilledCount() {
        filledLabel.setText("Filled slots: " + filledSlots);
    }

    protected void squareStyle() {
        // Top left 3x3 grid
        squareDisplay[2][0].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 2, Color.BLACK));
        squareDisplay[2][1].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 2, Color.BLACK));
        squareDisplay[2][2].setBorder(BorderFactory.createMatteBorder(0, 0, 2, 2, Color.BLACK));
        squareDisplay[0][2].setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));
        squareDisplay[1][2].setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));

        // Top middle 3x3 grid
        squareDisplay[3][2].setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));
        squareDisplay[4][2].setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));
        squareDisplay[5][2].setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));

        // Top right 3x3 grid
        squareDisplay[6][0].setBorder(BorderFactory.createMatteBorder(0, 2, 0, 0, Color.BLACK));
        squareDisplay[6][1].setBorder(BorderFactory.createMatteBorder(0, 2, 0, 0, Color.BLACK));
        squareDisplay[6][2].setBorder(BorderFactory.createMatteBorder(0, 2, 2, 0, Color.BLACK));
        squareDisplay[7][2].setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));
        squareDisplay[8][2].setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));

        // Middle left 3x3 grid
        squareDisplay[2][3].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 2, Color.BLACK));
        squareDisplay[2][4].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 2, Color.BLACK));
        squareDisplay[2][5].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 2, Color.BLACK));

        // Middle right 3x3 grid
        squareDisplay[6][3].setBorder(BorderFactory.createMatteBorder(0, 2, 0, 0, Color.BLACK));
        squareDisplay[6][4].setBorder(BorderFactory.createMatteBorder(0, 2, 0, 0, Color.BLACK));
        squareDisplay[6][5].setBorder(BorderFactory.createMatteBorder(0, 2, 0, 0, Color.BLACK));

        // Bottom left 3x3 grid
        squareDisplay[0][6].setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, Color.BLACK));
        squareDisplay[1][6].setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, Color.BLACK));
        squareDisplay[2][6].setBorder(BorderFactory.createMatteBorder(2, 0, 0, 2, Color.BLACK));
        squareDisplay[2][7].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 2, Color.BLACK));
        squareDisplay[2][8].setBorder(BorderFactory.createMatteBorder(0, 0, 0, 2, Color.BLACK));

        // Bottom middle 3x3 grid
        squareDisplay[3][6].setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, Color.BLACK));
        squareDisplay[4][6].setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, Color.BLACK));
        squareDisplay[5][6].setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, Color.BLACK));

        // Bottom right 3x3 grid
        squareDisplay[6][6].setBorder(BorderFactory.createMatteBorder(2, 2, 0, 0, Color.BLACK));
        squareDisplay[6][7].setBorder(BorderFactory.createMatteBorder(0, 2, 0, 0, Color.BLACK));
        squareDisplay[6][8].setBorder(BorderFactory.createMatteBorder(0, 2, 0, 0, Color.BLACK));
        squareDisplay[7][6].setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, Color.BLACK));
        squareDisplay[8][6].setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, Color.BLACK));
    }
}
