package main.abstracts;

public abstract class Puzzle implements IPuzzle {
    protected int size;
    protected int sizeSqr;
    protected int[][] board;

    public int getSize() { return size; }
    public void setSize(int size) { this.size = size; }

    public int getSizeSqr() { return sizeSqr; }
    public void setSizeSqr(int sizeSqr) { this.sizeSqr = sizeSqr; }

    public int[][] getBoard() { return board; }
    public void setBoard(int[][] board) { this.board = board; }

    @Override
    public void initBoard() {
        for(int x = 0; x < getSize(); x++)
            for(int y = 0; y < getSize(); y++)
                getBoard()[x][y] = 0;
    }

    @Override
    public String printBoard() {
        StringBuilder builder = new StringBuilder();
        for (int y = 0; y < getSize(); y++) {
            for (int x = 0; x < getSize(); x++) {
                builder.append(getBoard()[x][y]);
                if (x != getSize() - 1)
                    builder.append(" ");
                if(x == 2 || x == 5)
                    builder.append("| ");
            }
            builder.append("\r\n");
            if(y == 2 || y == 5)
                builder.append("= = = X = = = X = = =\r\n");
        }
        return builder.toString();
    }
}
