package main.abstracts;

import main.objects.Sudoku;

public abstract class Grid {
    protected Sudoku sudoku;
    protected boolean checkIfSafe(int i, int j, int num) {
        return(validRow(i, num) && validCol(j, num) && unUsedInBox(i - i % sudoku.getSizeSqr(), j - j % sudoku.getSizeSqr(), num));
    }

    protected boolean validRow(int x, int num) {
        for(int y = 0; y < sudoku.getSize(); y++)
            if(sudoku.getBoard()[x][y] == num)
                return false;
        return true;
    }

    protected boolean validCol(int y, int num) {
        for(int x = 0; x < sudoku.getSize(); x++)
            if(sudoku.getBoard()[x][y] == num)
                return false;
        return true;
    }

    protected boolean unUsedInBox(int rowStart, int colStart, int num) {
        for(int i = 0; i < sudoku.getSizeSqr(); i++)
            for(int j = 0; j < sudoku.getSizeSqr(); j++)
                if(sudoku.getBoard()[rowStart + i][colStart + j] == num)
                    return false;
        return true;
    }

    protected int randomNumber(int num) {
        return (int) Math.floor((Math.random() * num + 1));
    }
}
