package main.abstracts;

public interface IPuzzle {
    void initBoard();
    String printBoard();
}
