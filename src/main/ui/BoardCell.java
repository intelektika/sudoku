package main.ui;

import javax.swing.*;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

import main.Global;

public class BoardCell extends JLabel {
    protected int x;
    protected int y;
    public BoardCell(int x, int y) {
        this.x = x;
        this.y = y;
        setText("");
        setFont(Global.calibreFont);
        setOpaque(true);
        setHorizontalAlignment(SwingConstants.CENTER);
        setBackground(Global.cellNormal);
        setPreferredSize(Global.cellDimension);
        addMouseListener(createMouseListener());
    }

    protected MouseAdapter createMouseListener() {
        return new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                if(!getText().equals("")) {
                    setText("");
                    Global.sudokuJFrame.turnCount++;
                    Global.sudokuJFrame.updateTurnCount();
                    return;
                }

                String text = JOptionPane.showInputDialog("Enter a number between 1-9:");
                if(text == null)
                    return;

                int newValue = Integer.parseInt(text);
                if(newValue < 1)
                    return;
                if(newValue > 9)
                    return;

                if(Global.solver.placeNumber(x, y, newValue)) {
                    Global.sudokuJFrame.turnCount++;
                    Global.sudokuJFrame.refreshBoard();
                    Global.sudokuJFrame.updateTurnCount();
                } else {
                    JOptionPane.showMessageDialog(null, "This number cannot be placed here!", "Alert", JOptionPane.WARNING_MESSAGE);
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                setBackground(Global.cellHover);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setBackground(Global.cellNormal);
            }
        };
    }
}
