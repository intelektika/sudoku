package main.ui;

import main.Global;
import main.abstracts.BoardFrame;

import javax.swing.*;
import java.awt.*;

public class SudokuJFrame extends BoardFrame {

    public SudokuJFrame() {
        this.sudoku = Global.sudoku;
        this.generator = Global.generator;
        this.solver = Global.solver;
        this.turnLabel = new JLabel("0");
        this.filledLabel = new JLabel("0");

        this.container = getContentPane();
        this.container.setLayout(new GridLayout(1, 2));
        this.turnCount = 0;
        this.solved = false;
        this.filledSlots = 0;

        setTitle("Sudoku Solver - Emilis Žvirblys PI20C");
        init();

        pack();
        setResizable(false);
        setContentPane(container);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
